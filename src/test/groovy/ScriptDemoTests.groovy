import org.junit.jupiter.api.Test
import static org.junit.jupiter.api.Assertions.assertEquals

class ScriptDemoTests {

    @Test
    void ok() {
        def groovyShell = new GroovyShell()
        def script = groovyShell.parse(new File('src/main/groovy/ScriptDemo.groovy'))
        String msg = script.run()
        assertEquals('Demo message completed', msg)
    }
}
